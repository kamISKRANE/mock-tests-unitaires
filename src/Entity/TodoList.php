<?php

namespace App\Entity;

use App\Service\MailService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodoListRepository")
 */
class TodoList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="todoList", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="todoList", orphanRemoval=true)
     */
    private $items;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item) && $this->canAddItem($item)) {
            $this->items[] = $item;
            $item->setTodoList($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {


        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getTodoList() === $this) {
                $item->setTodoList(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function canAddItem(Item $item){
        if($this->items->count() >= 10){
            throw new \RuntimeException('There is already 10/10 items in this todolist');
        }

        if(strlen($item->getContent()) > 1000){
            throw new \RuntimeException('Content have to be 1000 carac max');
        }

        foreach($this->items as $tmp_item){
            if($tmp_item->getName() == $item->getName()){
                throw new \RuntimeException('An item with name '. $item->getName() .' already exist');
            }
        }

        $lastItem = $this->items->last();
        if($lastItem && $lastItem->getCreatedAt()->modify('+30 minutes')->getTimestamp()  <= (new \Datetime('now'))->getTimestamp() ){
            throw new \RuntimeException('You need to respect 30 min between two creation of items');
        }

        if(!MailService::Send($this->getUser())){
            throw new \RuntimeException("Email can't be send to user");
        }

        return $item;
    }
}
