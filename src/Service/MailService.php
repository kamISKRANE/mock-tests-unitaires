<?php

namespace App\Service;

use Carbon\Carbon;

class MailService
{
    static function send($user){
        if(Carbon::now()->subYears(18)->isAfter($user->getBirthday())){
            return true;
        }

        return false;
    }
}