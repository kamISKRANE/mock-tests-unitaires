<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Item;
use App\Entity\Todolist;

class TodoListService {

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getTodolist() {
        return $this->user->getTodolist();
    }

    public function createTodolist() {

        if( !$this->user->isValid() ){
            throw new \RuntimeException("User not valid, you cannot create an TodoList !");
        }

        if( $this->user->getTodolist() instanceof Todolist){
            throw new \RuntimeException("todolist already assoc to an user");
        }
    }

    public function addItem(Item $item) {

        $todo = $this->user->getTodolist();

        if(!$todo instanceof Todolist){
            throw new \RuntimeException("User doesn't have a todolist");
        }

        if($todo->getItems()->count() >= 10) {
            throw new \RuntimeException("Todolist cannot have more than 10 item");
        }

        $last = $todo->getItems()->last();
        if($last instanceof Item && floor(((new \DateTime)->getTimestamp() - $last->getCreatedAt()->getTimestamp()) / 60 ) < 30){
            throw new \RuntimeException('You have to wait 30 minutes before two items add');
        }

        if($item->getContent() === null || strlen($item->getContent()) === 0){
            throw new \RuntimeException('Item content is null');
        }

        if(strlen($item->getContent()) > 1000){
            throw new \RuntimeException('Content max character limit exceed (1000max)');
        }

        foreach ($this->user->getTodolist()->getItems() as $anItem){
            if($anItem->getName() === $item->getName()){
                throw new \RuntimeException('Name not uniq');
            }
        }

        if($todo->canAddItem($item)){
            $todo->addItem($item);
            return true;
        }

        return false;
    }
}