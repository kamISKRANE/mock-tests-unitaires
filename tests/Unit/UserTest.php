<?php

namespace App\Tests;

use App\Entity\User;
use App\Service\MailService;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        $this->user = new User();

        $birthday = (new \DateTime())->modify('-20 years');

        $this->user->setFirstName('Jean');
        $this->user->setLastName('Dujardin');
        $this->user->setEmail('mail@mail.fr');
        $this->user->setBirthday($birthday);
        $this->user->setPassword('Motdepasse123');

        parent::setUp();
    }

    public function testIsValid()
    {
        $this->assertTrue($this->user->isValid());
    }

    public function testIsNotValidEmptyFirstName(){
        $this->user->setFirstName('');
        $this->assertFalse($this->user->isValid());
    }

    public function testIsNotValidEmptyLastName(){
        $this->user->setLastName('');
        $this->assertFalse($this->user->isValid());
    }

    //Email
    public function testIsNotValidEmptyEmail(){
        $this->user->setEmail('');
        $this->assertFalse($this->user->isValid());
    }
    public function testIsNotValidBadEmail(){
        $this->user->setEmail('mail');
        $this->assertFalse($this->user->isValid());
    }

    // Password
    public function testIsNotValidEmptyPassword(){
        $this->user->setPassword('');
        $this->assertFalse($this->user->isValid());
    }
    public function testIsNotValidTooShortPassword(){
        $this->user->setPassword('mdp!');
        $this->assertFalse($this->user->isValid());
    }
    public function testIsNotValidTooLongPassword(){
        $this->user->setPassword('voiciunmotdepassebienpluslongque40caracteresoups');
        $this->assertFalse($this->user->isValid());
    }

    //Birthday
    public function testIsNotValidTooYoungBirthday(){
        $this->user->setBirthday(new \DateTime('2010-09-20'));
        $this->assertFalse($this->user->isValid());
    }

    //Mail
    public function testIsNotOkSendMail(){
        $this->user->setBirthday(new \DateTime('2010-09-20'));
        $this->assertFalse(MailService::Send($this->user));
    }
}