<?php

namespace App\Tests;

use App\Entity\TodoList;
use App\Entity\User;
use App\Service\TodoListService;
use PHPUnit\Framework\TestCase;

class TodoListTest extends TestCase
{
    private $theUser;

    public function setUp() {

        $user = new User();

        $birthday = (new \DateTime())->modify('-19 years');

        $user->setFirstName('Christophe');
        $user->setLastName('DECHAVAN');
        $user->setEmail('cricri@dechavan.fr');
        $user->setBirthday($birthday);
        $user->setPassword('montmdptropdur');

        $todolist = new TodoList();
        $user->setTodolist($todolist);
        $todolist->setUser($user);

        return $user;
    }

    public function testAddTodolist() {
        $user = $this->setUp();
        $this->assertTrue( $user->getTodolist() instanceof Todolist );
    }

    public function testAddTodolistErrorUserNotValid() {
        $user = $this->setUp();
        $user->setEmail('coucou@gmail_com');

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('User not valid, you cannot create an TodoList !');

        (new TodoListService($user))->createTodolist();
    }

    public function testAddTodolistErrorTodolistAlreadyExist() {
        $user = $this->setUp();

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('todolist already assoc to an user');

        (new TodoListService($user))->createTodolist();
    }





}