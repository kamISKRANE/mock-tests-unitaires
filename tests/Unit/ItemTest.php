<?php

namespace App\Tests;

use Carbon\Carbon;
use App\Entity\User;
use App\Entity\Item;
use App\Entity\TodoList;
use App\Service\TodoListService;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    public function setUp() {

        $user = new User();
        $todolist = new Todolist();

        $user->setTodolist($todolist);
        $todolist->setUser($user);

        return new TodoListService($user);
    }

    public function testAddItem() {
        $todolistService = $this->setUp();
        $item = new Item('name', 'Ipsum Lorem ');

        $this->assertTrue( $todolistService->addItem($item) );
    }

    public function testAddItemErrorTodolistMissing() {
        $todolistService = $this->setUp();
        $todolistService->getTodolist()->getUser()->setTodolist(null);
        $item = new Item('name', 'Ipsum Lorem ');

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("User doesn't have a todolist");
        $todolistService->addItem($item);
    }

    public function testAddItemErrorTodolistFull() {
        $todolistService = $this->setUp();
        $item = new Item('name', 'Ipsum Lorem ');

        for ($i = 0; $i < 10; $i++){
            $item = new Item('name'.$i, 'Ipsum Lorem '.$i);
            $todolistService->getTodolist()->addItem($item);
        }

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Todolist cannot have more than 10 item');
        $todolistService->addItem($item);
    }

    public function testAddItemErrorTime() {
        $todolistService = $this->setUp();
        $item = new Item('name1', 'Ipsum Lorem 1');

        $lastItem = new Item('name2', 'Ipsum Lorem 2');
        $lastItem->setCreatedAt( (new \DateTime())->modify('-10 mins') );
        $todolistService->getTodolist()->addItem($lastItem);

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('You have to wait 30 minutes before two items add');
        $todolistService->addItem($item);
    }

    public function testAddItemErrorContentNull() {
        $todolistService = $this->setUp();
        $item = new Item('name', null);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Item content is null');
        $todolistService->addItem($item);
    }

    //ON l'A DEJA CELLE LA ...
    public function testAddItemErrorContentLength() {
        $todolistService = $this->setUp();

        $content = str_repeat('a', 1000 + 1);
        $item = new Item('name', $content);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Content max character limit exceed (1000max)');
        $todolistService->addItem($item);
    }

    public function testAddItemErrorNameNotUniq() {
        $todolistService = $this->setUp();
        $item1 = new Item('name1', 'Ipsum Lorem 1');
        $item1->setCreatedAt( (new \DateTime())->modify('-30 mins') );

        $item2 = new Item('name1', 'Ipsum Lorem 2');
        $todolistService->addItem($item1);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Name not uniq');
        $todolistService->addItem($item2);
    }

    public function testSendEmail() {
        $todolistService = $this->setUp();
        $item = new Item('name', 'Ipsum Lorem ');

        $this->assertTrue($todolistService->addItem($item) );
    }

    public function testIsTooLongContentlenghtFalse(){
        $item = new Item('name', 'Ipsum Lorem ');
        //Repeat 1001 Time the word "A"
        $item->setContent(str_repeat("A", 1001));
        $this->assertFalse($item->isValid());
    }

}




/*
 *
 *
    public function testIsValid()
    {
        $this->assertTrue($this->item->isValid());
    }

    //Name
    public function testIsEmptyNameFalse(){
        $this->item->setName('');
        $this->assertFalse($this->item->isValid());
    }

    //Content

 */