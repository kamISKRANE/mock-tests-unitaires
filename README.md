## Composer
```composer install ```

## Run All tests :
```./bin/phpunit tests/Unit/*```
#### Or
```./bin/phpunit tests/Unit/*```

### Link:
#### https://phpunit.de/
#### https://phpunit.readthedocs.io/fr/latest/writing-tests-for-phpunit.html
#### https://github.com/sebastianbergmann/phpunit
